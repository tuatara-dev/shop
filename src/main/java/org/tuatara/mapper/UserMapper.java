package org.tuatara.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.tuatara.domain.User;

public interface UserMapper {

	@Select("SELECT * FROM user WHERE login=#{login}")
	@Results({
		@Result(id=true, property="userId", column="user_id"),
		@Result(property="login", column="login"),
		@Result(property="password", column="password"),
		@Result(property="name", column="name"),
		@Result(property="status", column="status"),
		@Result(property="isAdmin", column="is_admin")
	})
	public User getUser(String login);
	
	@Select("SELECT * FROM user")
	@Results({
		@Result(id=true, property="userId", column="user_id"),
		@Result(property="login", column="login"),
		@Result(property="password", column="password"),
		@Result(property="name", column="name"),
		@Result(property="status", column="status"),
		@Result(property="isAdmin", column="is_admin")
	})
	public List<User> getAllUsers();
	
	@Insert("INSERT INTO user(login, password, name) values(#{login}, #{password}, #{name})")
	@Options(useGeneratedKeys=true, keyProperty="userId")
	public void insertUser(User user);
	
	@Update("UPDATE user SET status=#{status}, login=#{login}, password=#{password}, name=#{name} "
			+ "WHERE user_id=#{userId}")
	public void updateUser(User user);
	
	@Delete("DELETE FROM user WHERE user_id=#{userId}")
	public void deleteUser(User user);
	
}
