package org.tuatara.presenter;

import java.util.List;

import org.tuatara.dao.CategoryDAO;
import org.tuatara.dao.LotDAO;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;

public class NewLotComponentHandler {

	
	public void insertNewLot(Lot lot){
		new LotDAO().insertLot(lot);
	}
	
	public List<Category> getCategories(){
		return new CategoryDAO().getAllCategories();
	}
	
	public Category getCategory(String categoryName){
		for(Category category : getCategories()){
			if(categoryName.equals(category.getName())){
				return category;
			}
		}
		return null;
	}
}
