package org.tuatara.presenter;

import java.util.List;

import org.tuatara.dao.CategoryDAO;
import org.tuatara.dao.LotDAO;
import org.tuatara.dao.UserDAO;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;
import org.tuatara.domain.User;

public class AdminPresenter {

	public List<User> getAllUsers(){
		return new UserDAO().getAllUsers();
	}
	
	public void updateUser(User user){
		new UserDAO().updateUser(user);
	}
	
	public List<Category> getAllCategories(){
		return new CategoryDAO().getAllCategories();
	}
	
	public void addNewLot(Lot lot){
		new LotDAO().insertLot(lot);
	}
}
