package org.tuatara.view.component;

import java.io.File;
import java.util.Iterator;

import org.tuatara.ShopUI;
import org.tuatara.presenter.HeaderComponentHandler;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class HeaderComponent extends CustomComponent{

	private Link   logo;
	private Button singInButton;
	private Button singUpButton;
	private Button logOutButton;
	private Button cartButton;
	private Button adminButton;
	
	private HeaderComponentHandler handler;
	
	public HorizontalLayout getComponent(){
		
		handler = new HeaderComponentHandler(this);
		
		// Load logo
		FileResource resource = new FileResource(new File(ShopUI.BASEPATH +"/img/logo.png"));
		logo = new Link(null, new ExternalResource(ShopUI.URL));
		logo.setIcon(resource);
		
		// Create buttons
		singInButton = new Button("Sing In");
		singInButton.addClickListener(event -> handler.showLoginWindow());
		
		singUpButton = new Button("Sing Up");
		singUpButton.addClickListener(event -> handler.showRegisterWindow());
		
		logOutButton = new Button("Log Out");
		logOutButton.addClickListener(event -> handler.doLogOut());
		
		cartButton = new Button("Cart");
		cartButton.addClickListener(event -> handler.goToCartView());
		
		adminButton = new Button("Admin Management");
		adminButton.addClickListener(event -> handler.goToAdminManagementView());
		
		HorizontalLayout buttons = new HorizontalLayout(singInButton, singUpButton, 
				cartButton, adminButton, logOutButton);
		
		// Set buttons style
		for(Iterator<Component> iterate = buttons.iterator(); iterate.hasNext();){
			iterate.next().addStyleName(ValoTheme.BUTTON_LINK);
		}
		
		HorizontalLayout header = new HorizontalLayout(logo, buttons);
		header.setWidth("100%");
		header.setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);
		
		return header;
	}

	public void setUnsignedMode(){
		
		singInButton.setVisible(true);
		singUpButton.setVisible(true);
		logOutButton.setVisible(false);
		cartButton.setVisible(false);
		adminButton.setVisible(false);
	}
	
	public void setClientMode(){
		
		singInButton.setVisible(false);
		singUpButton.setVisible(false);
		logOutButton.setVisible(true);
		cartButton.setVisible(true);
		adminButton.setVisible(false);
	}
	
	public void setAdminMode(){
		
		singInButton.setVisible(false);
		singUpButton.setVisible(false);
		logOutButton.setVisible(true);
		cartButton.setVisible(false);
		adminButton.setVisible(true);
	}
}
