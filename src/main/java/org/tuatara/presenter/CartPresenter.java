package org.tuatara.presenter;

import java.util.List;

import org.tuatara.dao.LotDAO;
import org.tuatara.domain.Lot;
import org.tuatara.domain.User;
import org.tuatara.service.AuthenticationService;
import org.tuatara.view.FrontView;

import com.vaadin.ui.UI;

public class CartPresenter {

	private User user;
	
	public CartPresenter(){
		// Check user session
		if(AuthenticationService.isUserAuthenticated()){
			user = AuthenticationService.getUser();
		} else {
			UI.getCurrent().getNavigator().navigateTo(FrontView.VIEW_NAME);
		}
	}
	
	public List<Lot> getLots(){
		
		return new LotDAO().getCart(user.getUserId());
	}
	
	public void removeLotFromCart(Lot lot){
		
		new LotDAO().deleteLotFromCart(user.getUserId(), lot.getLotId());
	}
	
	public void checkout(){

		for(Lot lot : getLots()){
			removeLotFromCart(lot);
		}
		// Send email
		// new EmailService().send(user.getLogin(), "Sombra Shop", text);
	}
}
