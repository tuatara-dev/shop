package org.tuatara.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;

public interface CategoryMapper {

	@Select("SELECT * FROM category")
	@Results({
		@Result(id=true, property="categoryId", column="category_id"),
		@Result(property="name", column="category_name"),
	})
	public List<Category> getAllCategories();
	
	@Select("SELECT * FROM category WHERE category_id=#{lotId}")
	@Results({
		@Result(id=true, property="categoryId", column="category_id"),
		@Result(property="name", column="category_name"),
	})
	public Category getCategory(Lot lot);
	
	@Insert("INSERT INTO category(category_name) values(#{name})")
	@Options(useGeneratedKeys=true, keyProperty="categoryId")
	public void insertCategory(Category category);
	
	@Delete("DELETE FROM category WHERE category_id=#{categoryId}")
	public void deleteCategory(Category category);
	
}
