package org.tuatara.view;

import java.util.List;

import org.tuatara.domain.User;
import org.tuatara.presenter.AdminPresenter;
import org.tuatara.view.component.HeaderComponent;
import org.tuatara.view.component.NewLotComponent;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@SpringView(name = AdminView.VIEW_NAME)
public class AdminView extends CustomComponent implements View{

	public static final String VIEW_NAME = "admin";
	
	private HeaderComponent header;
	private NewLotComponent newLot;
	
	private AdminPresenter presenter;
	
	private Table usersTable;
	
	public AdminView(){
		
		presenter = new AdminPresenter();
		generateUI();
	}
	
	private void generateUI(){
		
		header = new HeaderComponent();
		newLot = new NewLotComponent();

		usersTable = new Table();
		usersTable.addContainerProperty("Login", TextField.class, null);
		usersTable.addContainerProperty("Password", TextField.class, null);
		usersTable.addContainerProperty("Name", TextField.class, null);
		usersTable.addContainerProperty("Status", CheckBox.class, null);
		usersTable.setColumnAlignment("Status", Align.CENTER);
		usersTable.setHeight("400px");
		usersTable.setWidth("100%");
		fillUsersTable();
		
		TabSheet adminsTabSheet = new TabSheet();
		adminsTabSheet.addTab(usersTable, "Users");
		adminsTabSheet.addTab(newLot.getComponent(), "New Lot");
		
		VerticalLayout root = new VerticalLayout(header.getComponent(), adminsTabSheet);
		root.setSizeFull();
		root.setSpacing(true);
		root.setMargin(true);
		
		header.setAdminMode();
		
		setCompositionRoot(root);
	}
	
	private void fillUsersTable(){
		
		List<User> users = presenter.getAllUsers();
		
		for(User user : users){
			
			TextField login = new TextField();
			login.setValue(user.getLogin());
			login.setSizeFull();
			login.addValueChangeListener(event -> {
				user.setLogin(login.getValue());
				presenter.updateUser(user);
			});
			TextField password = new TextField();
			password.setSizeFull();
			password.setValue(user.getPassword());
			password.addValueChangeListener(event -> {
				user.setPassword(password.getValue());
				presenter.updateUser(user);
			});
			TextField name = new TextField();
			name.setValue(user.getName());
			name.setSizeFull();
			name.addValueChangeListener(event -> {
				user.setName(name.getValue());
				presenter.updateUser(user);
			});
			CheckBox userStatus = new CheckBox("", user.getStatus());
			userStatus.addValueChangeListener(event -> {
				user.setStatus(userStatus.getValue());
				presenter.updateUser(user);
				});
			
			usersTable.addItem(new Object[]{
					login,
					password,
					name,
					userStatus
			}, user.getUserId());
			
		}
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}