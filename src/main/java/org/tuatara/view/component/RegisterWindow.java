package org.tuatara.view.component;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.tuatara.presenter.RegisterWindowHandler;

import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class RegisterWindow extends Window{

	private TextField 		loginField;
	private TextField 		nameField;
    private PasswordField 	passwordField;
	private PasswordField 	repeatedField;
	
	private Button registerButton;
	
	private List<AbstractTextField> fields;
	
	private RegisterWindowHandler presenter;
	
	public RegisterWindow(){
		
		presenter = new RegisterWindowHandler(this);
		generateUI();
	}
	
	 private void generateUI(){
	    
		setModal(true);
		this.setHeight("500px");
		this.setWidth("400px");
		 
    	// Create the name input field
    	nameField = new TextField("Name:");
    	nameField.setWidth("300px");
    	nameField.setRequired(true);
    	nameField.setValue("");
    	nameField.setNullRepresentation("");
    	
    	// Create the login input field
    	loginField = new TextField("Email:");
    	loginField.setWidth("300px");
    	loginField.setRequired(true);
		loginField.setInputPrompt("Your email (eg. qwerty@email.com)");
		loginField.addValidator(new EmailValidator(
                "Username must be an email address"));
		loginField.setInvalidAllowed(false);
        
        // Create the password input field
		passwordField = new PasswordField("Password:");
		passwordField.setWidth("300px");
		passwordField.addValidator(new StringLengthValidator(
        		"Password length must be between 8 and 100 characters", 8, 100, false));
		passwordField.setRequired(true);
		
		// Create the password checker input field
		repeatedField = new PasswordField("Password:");
		repeatedField.setWidth("300px");
		repeatedField.addValidator(new StringLengthValidator(
        		"Password length must be between 8 and 100 characters", 8, 100, false));
		repeatedField.setRequired(true);

    	fields = (List<AbstractTextField>) Arrays.asList(new AbstractTextField[]{
        				nameField,
        				loginField,
        				passwordField,
        				repeatedField
        				});
    	
    	// Create register button
    	registerButton = new Button("Register");
    	registerButton.addClickListener(event -> presenter.doRegister());
    	
    	// Add components to the root component
        VerticalLayout root = new VerticalLayout(nameField, loginField, passwordField, 
        		repeatedField, registerButton);
        root.setSizeFull();
        root.setSpacing(true);
        root.setMargin(new MarginInfo(true));
        
        // set alignment for all components
        Iterator<Component> it = root.iterator();
        while(it.hasNext()){
        	root.setComponentAlignment(it.next(), Alignment.MIDDLE_CENTER);
        }

        setContent(root);
		this.center();
			
	 }

	public TextField getLoginField() {
		return loginField;
	}

	public TextField getNameField() {
		return nameField;
	}

	public PasswordField getPasswordField() {
		return passwordField;
	}

	public PasswordField getRepeatedField() {
		return repeatedField;
	}

	public Button getRegisterButton() {
		return registerButton;
	}

	public List<AbstractTextField> getFields() {
		return fields;
	}

}
