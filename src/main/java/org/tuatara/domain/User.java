package org.tuatara.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	@Column(name = "name")
	private String name;
	@Column(name = "status")
	private boolean status;
	@Column(name = "is_admin")
	private boolean isAdmin;
	@ElementCollection
	private List<Lot> cart = new ArrayList<>();
	
	public User(){}

	public User(String login, String password, String name,
			boolean status, boolean isAdmin) {
		super();
		this.login = login;
		this.password = password;
		this.name = name;
		this.status = status;
		this.isAdmin = isAdmin;
	}

	public Integer getUserId() {
		return userId;
	}


	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public boolean getStatus() {
		return status;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public List<Lot> getCart() {
		return cart;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setCart(List<Lot> cart) {
		this.cart = cart;
	}

}
