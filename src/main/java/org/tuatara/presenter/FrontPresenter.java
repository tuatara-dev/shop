package org.tuatara.presenter;

import java.util.List;

import org.tuatara.dao.CategoryDAO;
import org.tuatara.dao.LotDAO;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;
import org.tuatara.service.AuthenticationService;
import org.tuatara.view.component.HeaderComponent;

public class FrontPresenter {

	public void checkUserSession(HeaderComponent header){
		
		if(AuthenticationService.isUserAuthenticated()){
			if(AuthenticationService.getUser().isAdmin()){
				header.setAdminMode();
			} else {
				header.setClientMode();
			}
		} else {
			header.setUnsignedMode();
		}
	}
	
	public List<Category> getCategories(){
		return new CategoryDAO().getAllCategories();
	}
	
	public List<Lot> getLots(Category category){
		return new LotDAO().getLotsByCategory(category.getCategoryId());
	}
	
	public List<Lot> getLots(){
		return new LotDAO().getAllLots();
	}
}
