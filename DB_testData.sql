INSERT INTO user (login, password, name, is_admin) 
VALUES('maria@gmail.com', '3141569', 'Maria', false);
INSERT INTO user (login, password, name, is_admin) 
VALUES('aleksandr.kovalenko.ua@gmail.com', '3141569', 'Aleksandr', false);
INSERT INTO user (login, password, name, is_admin) 
VALUES('admin@gmail.com', '3141569', 'Admin', true);
INSERT INTO user (login, password, name, is_admin) 
VALUES('slava@gmail.com', '1111111', 'Slava', false);

insert into history values(1, "Circle", 55);
insert into history values(2, "Circle", 55);
insert into history values(3, "Circle", 55);
insert into history values(4, "Square", 55);
insert into history values(5, "Square", 55);

INSERT INTO category (category_name) 
VALUES('HP');
INSERT INTO category (category_name) 
VALUES('Acer');
INSERT INTO category (category_name) 
VALUES('Dell');
INSERT INTO category (category_name) 
VALUES('Lenovo');
INSERT INTO category (category_name) 
VALUES('Samsung');

INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('Dell Inspiron 3531', '/img/Dell Inspiron 3531 (Custom).jpg', '/img/Dell Inspiron 3531.jpg', 6426, true, 3, 'Экран 15.6" (1366x768) HD LED, глянцевый / Intel Celeron N2830 (2.16 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics / Без ОД / Wi-Fi 802.11b/g/n / веб-камера HD / Windows 8.1 / 2.12 кг / черный');
INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('Lenovo G500G', '/img/Lenovo G500G (Custom).jpg', '/img/Lenovo G500G.jpg', 6988, true, 5, 'Экран 15.6" (1366x768) HD LED, глянцевый / Intel Pentium 2020M (2.4 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics / DVD+/-RW / LAN / Wi-Fi / веб-камера / DOS / 2.6 кг / черный');
INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('Dell Inspiron 3542', '/img/Dell Inspiron 3542 (Custom).jpg', '/img/Dell Inspiron 3542.jpg', 7399, true, 3, 'Экран 15.6" (1366x768) HD LED, глянцевый / Intel Pentium 3558U (1.7 ГГц) / RAM 4 ГБ / HDD 500 ГБ / nVidia GeForce 820M, 2 ГБ / DVD+/-RW / LAN / Wi-Fi / Bluetooth 4.0 / веб-камера HD / Linux / 2.4 кг / черный');
INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('Acer Packard Bell ENTG71BM-CE5B', '/img/Acer Packard Bell ENTG71BM-CE5B (Custom).jpg', '/img/Acer Packard Bell ENTG71BM-CE5B.jpg', 5999, true, 2, 'Экран 15.6" (1366x768) HD LED, глянцевый / Intel Celeron N2840 (2.16 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / Linpus / 2.4 кг / черный');
INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('Acer Aspire V5-552G-85554G50AKK', '/img/Acer Aspire V5-552G-85554G50AKK (Custom).jpg', '/img/Acer Aspire V5-552G-85554G50AKK.jpg', 9299, true, 2, 'Экран 15.6” (1366x768) HD LED, матовый / AMD Quad-Core A8-5557M (2.1-3.1 ГГц) / RAM 4 ГБ / HDD 500 ГБ / AMD Radeon HD 8550G + AMD Radeon HD 8750M, 2 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 2.2 кг / черный');
INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('HP ProBook 470', '/img/HP ProBook 470 (Custom).jpg', '/img/HP ProBook 470.jpg', 11555, true, 1, 'Экран 17.3" (1600x900) HD+ LED, матовый / Intel Core i3-4000M (2.4 ГГц) / RAM 4 ГБ / HDD 500 ГБ / AMD Radeon HD 8750M, 1 ГБ / DVD+/-RW / LAN / Wi-Fi / Bluetooth / веб-камера / Linux / 3 кг / сумка');
INSERT INTO lot (lot_name, icon, photo, price, status, category_id, description) 
VALUES('Lenovo G710G', '/img/Lenovo G710G (Custom).jpg', '/img/Lenovo G710G.jpg', 8832, true, 4, 'Экран 17.3" (1600x900) HD+ LED, глянцевый / Intel Pentium 3550M (2.3 ГГц) / RAM 4 ГБ / HDD 500 ГБ / Intel HD Graphics / DVD±RW / LAN / Bluetooth / Wi-Fi / веб-камера / DOS / 2.9 кг / черный');

INSERT INTO cart (user_id, lot_id) 
VALUES(2, 2);
INSERT INTO cart (user_id, lot_id) 
VALUES(2, 3);
INSERT INTO cart (user_id, lot_id) 
VALUES(2, 7);
INSERT INTO cart (user_id, lot_id) 
VALUES(4, 2);