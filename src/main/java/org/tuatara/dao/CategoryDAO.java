package org.tuatara.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;
import org.tuatara.mapper.CategoryMapper;
import org.tuatara.service.MyBatisUtil;


public class CategoryDAO implements CategoryMapper{

	@Override
	public List<Category> getAllCategories() {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CategoryMapper categoryMapper = sqlSession.getMapper(CategoryMapper.class);
			List<Category> categories = categoryMapper.getAllCategories();
			return categories;
		}
	}

	@Override
	public Category getCategory(Lot lot) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CategoryMapper categoryMapper = sqlSession.getMapper(CategoryMapper.class);
			Category category = categoryMapper.getCategory(lot);
			return category;
		}
	}
	
	@Override
	public void insertCategory(Category category) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CategoryMapper categoryMapper = sqlSession.getMapper(CategoryMapper.class);
			categoryMapper.insertCategory(category);
			sqlSession.commit();
		}
	}

	@Override
	public void deleteCategory(Category category) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CategoryMapper categoryMapper = sqlSession.getMapper(CategoryMapper.class);
			categoryMapper.deleteCategory(category);
			sqlSession.commit();
		}
	}

}
