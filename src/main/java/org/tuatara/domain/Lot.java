package org.tuatara.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Embeddable
public class Lot {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "lot_id")
	private Integer lotId;
	@Column(name = "lot_name")
	private String name;
	@Column(name = "icon")
	private String icon;
	@Column(name = "photo")
	private String photo;
	@Column(name = "price")
	private int price;
	@Column(name = "description")
	private String description;
	@Embedded
	private Category category;
	@Column(name = "status")
	private boolean status;
	
	public Lot(){}

	public Lot(String name, String icon, String photo, int price,
			String description, Category category, boolean status) {
		super();
		this.name = name;
		this.icon = icon;
		this.photo = photo;
		this.price = price;
		this.description = description;
		this.category = category;
		this.status = status;
	}


	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getLotId() {
		return lotId;
	}

	public String getName() {
		return name;
	}

	public String getPhoto() {
		return photo;
	}

	public int getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}

	public Category getCategory() {
		return category;
	}

	public boolean getStatus() {
		return status;
	}

	public void setLotId(Integer lotId) {
		this.lotId = lotId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object lot){
		return Lot.class.isInstance(lot) && 
				this.lotId.equals(((Lot) lot).getLotId());
	}
	
}
