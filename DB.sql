SET NAMES 'utf8';
SET CHARACTER SET UTF8;

DROP TRIGGER IF EXISTS lot_delete;
DROP TRIGGER IF EXISTS user_delete;
DROP TRIGGER IF EXISTS category_delete;

DROP TABLE IF EXISTS cart;
DROP TABLE IF EXISTS lot;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS user;

CREATE TABLE user (
  user_id int(10) unsigned NOT NULL auto_increment,
  login varchar(50) NOT NULL UNIQUE,
  password varchar(50) NOT NULL,
  name varchar(50) NOT NULL,
  status boolean default true,
  is_admin boolean default false,
  CONSTRAINT pk_user PRIMARY KEY (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE category(
  category_id int(10) unsigned NOT NULL auto_increment,
  category_name varchar(100) NOT NULL UNIQUE,
  PRIMARY KEY(category_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE lot (
  lot_id int(10) unsigned NOT NULL auto_increment,
  lot_name varchar(50) NOT NULL,
  icon varchar(50) default NULL,
  photo varchar(50) default NULL,
  price int(10) unsigned NOT NULL,
  status boolean default true,
  category_id int(10) unsigned NOT NULL,
  description Text NOT NULL,
  PRIMARY KEY(lot_id),
  CONSTRAINT fk_category_id FOREIGN KEY (category_id) REFERENCES category (category_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE cart(
  cart_id int(10) unsigned NOT NULL auto_increment,
  user_id int(10) unsigned NOT NULL,
  lot_id int(10) unsigned NOT NULL,
  PRIMARY KEY(cart_id),
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES user (user_id),
  CONSTRAINT fk_lot_id FOREIGN KEY (lot_id) REFERENCES lot (lot_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

delimiter //
CREATE TRIGGER lot_delete 
BEFORE DELETE ON lot 
FOR EACH ROW 
BEGIN 
DELETE FROM cart WHERE cart.lot_id = old.lot_id; 
END;//
delimiter ;

delimiter //
CREATE TRIGGER user_delete 
BEFORE DELETE ON user 
FOR EACH ROW 
BEGIN 
DELETE FROM cart WHERE cart.user_id = old.user_id; 
END;
//
delimiter ;

delimiter //
CREATE TRIGGER category_delete 
BEFORE DELETE ON category 
FOR EACH ROW 
BEGIN 
DELETE FROM lot WHERE lot.category_id = old.category_id; 
END;
//
delimiter ;