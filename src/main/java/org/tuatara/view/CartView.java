package org.tuatara.view;

import org.tuatara.domain.Lot;
import org.tuatara.presenter.CartPresenter;
import org.tuatara.view.component.HeaderComponent;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
@SpringView(name = CartView.VIEW_NAME)
public class CartView extends CustomComponent implements View{

	public static final String VIEW_NAME = "cart";
	
	private HeaderComponent header;
	
	private CartPresenter presenter;
	
	private Table cartTable;
	private Label totalPriceLabel;
	
	private int totalPrice = 0;
	
	public CartView(){
		
		presenter = new CartPresenter();
		generateUI();
	}
	
	private void generateUI(){
		
		header = new HeaderComponent();
		
		cartTable = new Table();
		cartTable.addContainerProperty("Name", String.class, null);
		cartTable.addContainerProperty("Status", String.class, null);
		cartTable.addContainerProperty("Price", Integer.class, null);
		cartTable.addContainerProperty("Remove", Button.class, null);
		cartTable.setHeight("400px");
		cartTable.setWidth("100%");
		cartTable.setReadOnly(true);
		
		fillCart();
		
		totalPriceLabel = new Label("Total Price: " + totalPrice);

		Button checkoutButton = new Button("checkout");
		checkoutButton.addClickListener(event -> checkout());
		
		VerticalLayout root = new VerticalLayout(header.getComponent(), cartTable, totalPriceLabel, checkoutButton);
		root.setSizeFull();
		root.setSpacing(true);
		root.setMargin(true);
		root.setComponentAlignment(totalPriceLabel, Alignment.MIDDLE_CENTER);
		root.setComponentAlignment(checkoutButton, Alignment.MIDDLE_CENTER);
		header.setClientMode();
		
		setCompositionRoot(root);
	}
	
	private void fillCart(){
		
		for(Lot lot : presenter.getLots()){

			String status = lot.getStatus()? "available" : "not available";
			
			Button removeButton = new Button("Remove");
			removeButton.setSizeFull();
			removeButton.setStyleName(ValoTheme.BUTTON_DANGER);
			removeButton.addClickListener(event -> removeLot(lot));
			
			cartTable.addItem(new Object[]{
					lot.getName(),
					status,
					lot.getPrice(),
					removeButton
			}, lot.getLotId());
			
			totalPrice += lot.getPrice();
		}
	}
	
	private void removeLot(Lot lot){
		
		cartTable.removeItem(lot.getLotId());
		
		totalPrice -= lot.getPrice();
		totalPriceLabel.setValue("Total Price: " + totalPrice);
		
		presenter.removeLotFromCart(lot);
	}
	
	private void checkout(){
		
		cartTable.removeAllItems();
		totalPriceLabel.setValue("Total Price: 0");
		
		presenter.checkout();
		
		Notification.show("Thanks for shopping", "",
                Notification.Type.HUMANIZED_MESSAGE);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
