package org.tuatara.view.component;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.tuatara.ShopUI;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;
import org.tuatara.presenter.NewLotComponentHandler;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class NewLotComponent extends CustomComponent{
	
	private static final String UPLOAD_ERROR = "Upload Failed, try again";
	
	private TextField nameTextField;
	private Upload iconUpload;
	private Upload photoUpload;
	private TextField priceTextField;
	private ComboBox categoryComboBox;
	private TextArea descriptionTextArea;
	private Button createButton;
	
	private NewLotComponentHandler presenter;
	
	public NewLotComponent(){
		
		presenter = new NewLotComponentHandler();
		generateUI();
	}
	
	private void generateUI(){
		
		nameTextField = new TextField("Name");
		priceTextField = new TextField("Price");
		categoryComboBox = new ComboBox("Category");
		categoryComboBox.setNullSelectionAllowed(false);
		fillCategories();
		descriptionTextArea = new TextArea("Description");
		
		iconUpload = new Upload("", new ImageReceiver());
		iconUpload.setImmediate(true);
		iconUpload.setButtonCaption("Upload Icon");
		iconUpload.addSucceededListener(
				event -> event.getUpload().setCaption(event.getFilename())
				);
		iconUpload.addFailedListener(
				event -> event.getUpload().setCaption(UPLOAD_ERROR)
				);
		
		photoUpload = new Upload("", new ImageReceiver());
		photoUpload.setImmediate(true);
		photoUpload.setButtonCaption("Upload Photo");
		photoUpload.addSucceededListener(
				event -> event.getUpload().setCaption(event.getFilename())
				);
		photoUpload.addFailedListener(
				event -> event.getUpload().setCaption(UPLOAD_ERROR)
				);
		
		HorizontalLayout uploads = new HorizontalLayout(iconUpload, photoUpload);
		iconUpload.setWidth("150px");
		photoUpload.setWidth("150px");
		uploads.setComponentAlignment(iconUpload, Alignment.MIDDLE_CENTER);
		uploads.setComponentAlignment(photoUpload, Alignment.MIDDLE_CENTER);
		uploads.setSizeFull();
		createButton = new Button("Create");
		createButton.addClickListener(event -> saveLot());
		
		VerticalLayout root = new VerticalLayout(nameTextField, uploads, 
				priceTextField, categoryComboBox, descriptionTextArea, createButton);
		root.setSpacing(true);
		
		for(Component component : root){
			component.setWidth("350px");
			root.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
		}
		createButton.setWidth("100px");
		setCompositionRoot(root);
	}
	
	private void saveLot(){
		
		if(categoryComboBox.getValue() == null ||
				UPLOAD_ERROR.equals(iconUpload.getCaption()) ||
				UPLOAD_ERROR.equals(photoUpload.getCaption())){
			
			Notification.show("Invalid Data", "",
	                Notification.Type.ERROR_MESSAGE);
		} else {
		
			Lot lot = new Lot(
					nameTextField.getValue(),
					"/img/" + iconUpload.getCaption(),
					"/img/" + photoUpload.getCaption(),
					Integer.parseInt(priceTextField.getValue()),
					descriptionTextArea.getValue(),
					presenter.getCategory((String) categoryComboBox.getValue()),
					true);
			presenter.insertNewLot(lot);
			
			Notification.show("You created new Lot", "",
	                Notification.Type.HUMANIZED_MESSAGE);
		}
	}
	
	private void fillCategories(){
		for(Category category : presenter.getCategories()){
			categoryComboBox.addItem(category.getName());
		}
	}
	
	public Component getComponent(){
		return this;
	}
	
	private class ImageReceiver implements Receiver{
		
	    @Override
	    public OutputStream receiveUpload(String filename, String mimeType) {
		    FileOutputStream output = null;
		    try{
		    	output = new FileOutputStream(ShopUI.BASEPATH + "/img/" + filename);
		    } catch (FileNotFoundException e) {
		    	e.printStackTrace();
		    }
		    return output;
	    }

	};
	
	
}
