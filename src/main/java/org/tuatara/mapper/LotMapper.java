package org.tuatara.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.tuatara.domain.Lot;

public interface LotMapper {

	@Select("SELECT * FROM lot")
	@Results({
		@Result(id=true, property="lotId", column="lot_id"),
		@Result(property="name", column="lot_name"),
		@Result(property="icon", column="icon"),
		@Result(property="photo", column="photo"),
		@Result(property="price", column="price"),
		@Result(property="status", column="status"),
		@Result(property="description", column="description")
	})
	public List<Lot> getAllLots();
	
	@Select("SELECT * FROM lot WHERE category_id=#{categoryId}")
	@Results({
		@Result(id=true, property="lotId", column="lot_id"),
		@Result(property="name", column="lot_name"),
		@Result(property="icon", column="icon"),
		@Result(property="photo", column="photo"),
		@Result(property="price", column="price"),
		@Result(property="status", column="status"),
		@Result(property="description", column="description")
	})
	public List<Lot> getLotsByCategory(Integer categoryId);
	
	@Select("SELECT * FROM lot INNER JOIN cart ON lot.lot_id=cart.lot_id WHERE cart.user_id=#{userId}")
	@Results({
		@Result(id=true, property="lotId", column="lot_id"),
		@Result(property="name", column="lot_name"),
		@Result(property="icon", column="icon"),
		@Result(property="photo", column="photo"),
		@Result(property="price", column="price"),
		@Result(property="status", column="status"),
		@Result(property="description", column="description")
	})
	public List<Lot> getCart(Integer userId);
	
	@Insert("INSERT INTO lot(lot_name, icon, photo, price, status, category_id, description) "
			+ "values(#{name}, #{icon}, #{photo}, #{price}, #{status}, #{category.categoryId}, #{description})")
	@Options(useGeneratedKeys=true, keyProperty="userId")
	public void insertLot(Lot lot);
	
	@Insert("INSERT INTO cart(user_id, lot_id) values(#{userId}, #{lotId})")
	public void insertIntoTheCart(@Param("userId") int userId, @Param("lotId") int lotId);
	
	@Update("UPDATE lot SET lot_name=#{name}, icon=#{icon}, photo=#{photo}, price=#{price}, status=#{status}, "
			+ "description=#{description}, category_id=#{category.categoryId} WHERE lot_id=#{lotId}")
	public void updateLot(Lot lot);
	
	@Delete("DELETE FROM lot WHERE lot_id=#{lotId}")
	public void deleteLot(Lot lot);
	
	@Delete("DELETE FROM cart WHERE cart.user_id=#{userId} AND cart.lot_id=#{lotId}")
	public void deleteLotFromCart(@Param("userId") int userId, @Param("lotId") int lotId);
	
}
