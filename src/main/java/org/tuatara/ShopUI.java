package org.tuatara;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.tuatara.view.FrontView;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringUI
@Theme("valo")
@WebServlet(value = "/*", asyncSupported = true)
@VaadinServletConfiguration(productionMode = false, ui = ShopUI.class)
public class ShopUI extends UI{
	
	@Autowired
    private SpringViewProvider viewProvider;
	
	private static Navigator navigator;
	
	public static final String URL = "http://localhost:8080/";
	
	public static final String BASEPATH = 
			VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
	
	@Override
	protected void init(VaadinRequest vaadinRequest) {
		
		// Create navigator and go to Front view
		navigator = new Navigator(this, this);
		navigator.addProvider(viewProvider);
		navigator.navigateTo(FrontView.VIEW_NAME);
    }
	
}
