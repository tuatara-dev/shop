package org.tuatara.presenter;

import org.tuatara.dao.LotDAO;
import org.tuatara.domain.Lot;
import org.tuatara.service.AuthenticationService;

public class LotWindowHandler {

	
	public boolean isUserBanned(){
		return AuthenticationService.isUserAuthenticated() 
				&& AuthenticationService.getUser().getStatus();
	}
	
	public boolean isLotInTheCart(Lot lot){
		return AuthenticationService.isUserAuthenticated()
				&& new LotDAO().getCart(
						AuthenticationService.getUser().getUserId())
						.contains(lot);
	}
	
	public void addToCart(Lot lot){
		new LotDAO().insertIntoTheCart(AuthenticationService.getUser().getUserId(), 
				lot.getLotId());
	}
	
	public void removeFromCart(Lot lot){
		new LotDAO().deleteLotFromCart(AuthenticationService.getUser().getUserId(), 
				lot.getLotId());
	}
	
}
