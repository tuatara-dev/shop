package org.tuatara.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.tuatara.domain.User;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.UI;

public class AuthenticationService {
	
	public static void setUser(User user){
		
		UI.getCurrent().getSession().setAttribute("user", user);
		VaadinService.getCurrentRequest().getWrappedSession()
        .setAttribute("user", user);
	}
	
	public static User getUser(){
		
		return (User) UI.getCurrent().getSession().getAttribute("user");
	}
	
	public static void removeUser(){
		
		setUser(null);
	}
	
	
	public static boolean isUserAuthenticated(){
		
		return UI.getCurrent().getSession().getAttribute("user") != null;
	}
	
	public static boolean isPasswordCorrect(User user, String password){
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.matches(password, passwordEncoder.encode(user.getPassword()));
	}
}
