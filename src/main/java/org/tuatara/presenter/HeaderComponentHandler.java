package org.tuatara.presenter;

import org.tuatara.service.AuthenticationService;
import org.tuatara.view.AdminView;
import org.tuatara.view.CartView;
import org.tuatara.view.FrontView;
import org.tuatara.view.component.HeaderComponent;
import org.tuatara.view.component.LoginWindow;
import org.tuatara.view.component.RegisterWindow;

import com.vaadin.ui.UI;

public class HeaderComponentHandler {

	private HeaderComponent component;
	
	public HeaderComponentHandler(HeaderComponent component){
		this.component = component;
	}
	
	public void showLoginWindow(){
		UI.getCurrent().addWindow(new LoginWindow());
	}
	
	public void showRegisterWindow(){
		UI.getCurrent().addWindow(new RegisterWindow());
	}
	
	public void doLogOut(){
		AuthenticationService.removeUser();
		component.setUnsignedMode();
		UI.getCurrent().getNavigator().navigateTo(FrontView.VIEW_NAME);
	}
	
	public void goToCartView(){
		UI.getCurrent().getNavigator().addView(CartView.VIEW_NAME, new CartView());
		UI.getCurrent().getNavigator().navigateTo(CartView.VIEW_NAME);
	}
	
	public void goToAdminManagementView(){
		UI.getCurrent().getNavigator().addView(AdminView.VIEW_NAME, new AdminView());
		UI.getCurrent().getNavigator().navigateTo(AdminView.VIEW_NAME);
	}
	
}
