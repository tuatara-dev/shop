package org.tuatara.service.email;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailService {
	
	private Properties prop;
	
	public EmailService(){
		
		prop = new Properties();
		try(InputStream in = EmailService.class.getResourceAsStream("email.properties")){
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void send(String recipient, String subject, String text){
		
		Session session = Session.getInstance(prop,
		          new javax.mail.Authenticator() {
		            protected PasswordAuthentication getPasswordAuthentication() {
		                return new PasswordAuthentication(prop.getProperty("login"), prop.getProperty("password"));
		            }
		          });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(prop.getProperty("login")));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setText(text);

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}
	
}
