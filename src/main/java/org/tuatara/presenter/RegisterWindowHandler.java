package org.tuatara.presenter;

import org.tuatara.dao.UserDAO;
import org.tuatara.domain.User;
import org.tuatara.service.AuthenticationService;
import org.tuatara.view.component.RegisterWindow;

import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Notification;

public class RegisterWindowHandler {

	private RegisterWindow registerView;
	
	public RegisterWindowHandler(RegisterWindow registerView){
		
		this.registerView = registerView;
	}
	
	public void doRegister(){
		
		if(!checkFieldsValidation()){
			Notification.show("Input values isn't valid", Notification.Type.HUMANIZED_MESSAGE);
			return;
		} else {
			String userName = registerView.getNameField().getValue();
			String login = registerView.getLoginField().getValue();
			String password = registerView.getPasswordField().getValue();
			
			// create user
			User user = new User(login, password, userName, true, false);
			// insert to database
			new UserDAO().insertUser(user);
			// set user session
			AuthenticationService.setUser(user);
			
			// Refresh page
			JavaScript.getCurrent().execute("window.location.reload();");
			
			registerView.close();
		}
	}
	
	private boolean checkFieldsValidation(){
		
		String pass = registerView.getPasswordField().getValue();
		String repass = registerView.getRepeatedField().getValue();
		if(!pass.equals(repass)){
			return false;
		}
		
		for(AbstractTextField tf : registerView.getFields()){
			if(!tf.isValid()){
				return false;
			}
		}
		return true;
	}
}
