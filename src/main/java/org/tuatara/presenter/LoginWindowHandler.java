package org.tuatara.presenter;

import org.tuatara.dao.UserDAO;
import org.tuatara.domain.User;
import org.tuatara.service.AuthenticationService;
import org.tuatara.view.FrontView;
import org.tuatara.view.component.LoginWindow;

import com.vaadin.ui.JavaScript;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;

public class LoginWindowHandler {
	
	private LoginWindow window;
	
	public LoginWindowHandler(LoginWindow window){
		this.window = window;
	}
	
	public void doLogin(TextField loginField, PasswordField passwordField){
		if (!loginField.isValid() || !passwordField.isValid()) {
			Notification.show("Login or Password is't valid. Please try again.", Notification.Type.HUMANIZED_MESSAGE);
	    } else {
		    String login 	= loginField.getValue();
			String password = passwordField.getValue();
			
			User user = new UserDAO().getUser(login);
			
			if(user != null && AuthenticationService.isPasswordCorrect(user, password)){

				AuthenticationService.setUser(user);
				
				// go to front view
				FrontView view = new FrontView();
				if(user.isAdmin()){
					view.getHeader().setAdminMode();
				} else {
					view.getHeader().setClientMode();
				}
				
				// Refresh page
				JavaScript.getCurrent().execute("window.location.reload();");
				
				window.close();
				
			} else {
				Notification.show("Login Failed. Please try again.", Notification.Type.HUMANIZED_MESSAGE);
			}
	    }
	}
	
}
