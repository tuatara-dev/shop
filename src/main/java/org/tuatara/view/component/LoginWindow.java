package org.tuatara.view.component;

import java.util.Iterator;

import org.tuatara.presenter.LoginWindowHandler;

import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class LoginWindow extends Window{

	private TextField 		loginField;
	private PasswordField 	passwordField;
	private Button 			loginButton;
	
	private LoginWindowHandler presenter;
	
	public LoginWindow(){
		
		super();
		presenter = new LoginWindowHandler(this);
		generateUI();
	}
	
	private void generateUI(){
		
		setModal(true);
		this.setHeight("200px");
		this.setWidth("400px");
		
		// Create the login input field
		loginField = new TextField("Email:");
		loginField.setWidth("300px");
		loginField.setInputPrompt("Your email (eg. qwerty@email.com)");
		loginField.addValidator(new EmailValidator(
                "Username must be an email address"));
        
        // Create the password input field
		passwordField = new PasswordField("Password:");
		passwordField.setWidth("300px");
        passwordField.addValidator(new StringLengthValidator(
        		"Password must be at least 4 characters long", 4, Integer.MAX_VALUE, false));
        
        // Create login button
        loginButton = new Button("Login");
        loginButton.addClickListener(event -> presenter.doLogin(loginField, passwordField));
        
        // Add both to a panel
        VerticalLayout root = new VerticalLayout(loginField, passwordField, loginButton);
        root.setCaption("Please login to access the application or sing up.");
        root.setSpacing(true);
        root.setMargin(new MarginInfo(true));
        root.setSizeFull();
        
        Iterator<Component> it = root.iterator();
        while(it.hasNext()){
        	root.setComponentAlignment(it.next(), Alignment.MIDDLE_CENTER);
        }
        
        setContent(root);
		this.center();
	}
	
}
