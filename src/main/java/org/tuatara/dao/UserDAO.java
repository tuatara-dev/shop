package org.tuatara.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.tuatara.domain.User;
import org.tuatara.mapper.UserMapper;
import org.tuatara.service.MyBatisUtil;

public class UserDAO implements UserMapper{

	@Override
	public User getUser(String login) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			User user = userMapper.getUser(login);
			return user;
		}
	}

	@Override
	public List<User> getAllUsers() {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			List<User> users = userMapper.getAllUsers();
			return users;
		}
	}
	
	@Override
	public void insertUser(User user) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			
			userMapper.insertUser(user);
			sqlSession.commit();
		}
	}

	@Override
	public void updateUser(User user) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			
			userMapper.updateUser(user);
			sqlSession.commit();
		}
	}

	@Override
	public void deleteUser(User user) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			userMapper.deleteUser(user);
			sqlSession.commit();
		}
	}

}
