package org.tuatara.view;

import java.io.File;
import java.util.List;

import org.tuatara.ShopUI;
import org.tuatara.domain.Category;
import org.tuatara.domain.Lot;
import org.tuatara.presenter.FrontPresenter;
import org.tuatara.view.component.HeaderComponent;
import org.tuatara.view.component.LotWindow;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FileResource;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
@SpringView(name = FrontView.VIEW_NAME)
public class FrontView extends CustomComponent implements View{

	public static final String VIEW_NAME = "";
	
	private final FrontPresenter presenter;
	
	private HeaderComponent header;
	private TabSheet lotsTabSheet;
	
	// Max number of lots in the row
	private final int LOTS_NAMBER = 3;

	public FrontView(){
		
		presenter = new FrontPresenter();
		generateUI();
	}
	
	private void generateUI(){
		
		header = new HeaderComponent();
		
		lotsTabSheet = new TabSheet();
		
		// Fill Tabs for every categories
		for(Category category : presenter.getCategories()){
			
			fillTab(presenter.getLots(category), category.getName());
		}
		
		// Fill Tab for "All"
		List<Lot> lots = presenter.getLots();
		fillTab(lots, "All");
		
		Panel lotsPanel = new Panel(lotsTabSheet);
		lotsPanel.setSizeFull();
		
		VerticalLayout root = new VerticalLayout(header.getComponent(), lotsPanel);
		root.setSizeFull();
		root.setSpacing(true);
		root.setMargin(true);
		presenter.checkUserSession(header);
		setCompositionRoot(root);
	}
	
	private void fillTab(List<Lot> lots, String categoryName){
		
		int column, row = 0;
		
		GridLayout lotsLayout = new GridLayout(LOTS_NAMBER, (lots.size()/LOTS_NAMBER)+1);
		lotsLayout.setMargin(true);
		lotsLayout.setSizeFull();
		
		for(int i=0; i<lots.size(); i++){
			
			column = i/LOTS_NAMBER;
			row = column==1? row++ : row ;
			
			Lot lot = lots.get(i);
			Button lotButton = new Button(lot.getName());
			lotButton.setIcon(new FileResource(new File(ShopUI.BASEPATH + lot.getIcon())));
			lotButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			lotButton.addStyleName(ValoTheme.BUTTON_ICON_ALIGN_TOP);
			lotButton.setWidth("300px");
			lotButton.setHeight("240px");
			lotButton.setData(lot);
			lotButton.addClickListener(event -> showLot((Lot)lotButton.getData()));
			
			lotsLayout.setCursorX(column);
			lotsLayout.setCursorX(row);
			
			lotsLayout.addComponent(lotButton);
			lotsLayout.setComponentAlignment(lotButton, Alignment.TOP_CENTER);
		}
		
		lotsTabSheet.addTab(lotsLayout, categoryName);
	}
	
	private void showLot(Lot lot){
		UI.getCurrent().addWindow(new LotWindow(lot));
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

	public HeaderComponent getHeader() {
		return header;
	}
	
}
