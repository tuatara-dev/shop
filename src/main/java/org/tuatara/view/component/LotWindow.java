package org.tuatara.view.component;

import java.io.File;

import org.tuatara.ShopUI;
import org.tuatara.domain.Lot;
import org.tuatara.presenter.LotWindowHandler;

import com.vaadin.server.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class LotWindow extends Window{
	
	private Lot lot;
	
	private LotWindowHandler presenter;
	
	public LotWindow(Lot lot){
		
		super();
		this.lot = lot;
		presenter = new LotWindowHandler();
		generateUI();
	}
	
	private void generateUI(){
		
		this.setHeight("80%");
		this.setWidth("80%");
		
		// Load lot photo
		FileResource resource = new FileResource(
				new File(ShopUI.BASEPATH + lot.getPhoto()));
		Image photo = new Image();
		photo.setSource(resource);
		
		Label nameLabel = new Label(lot.getName());
		nameLabel.addStyleName("h2");
		if(lot.getStatus()){
			nameLabel.addStyleName("success");
		} else {
			nameLabel.addStyleName("failure");
		}
		
		TextArea descriptionTextArea = new TextArea();
		descriptionTextArea.setValue(lot.getDescription());
		descriptionTextArea.setReadOnly(true);
		descriptionTextArea.setSizeFull();
		
		Label priceLabel = new Label("Price: " + lot.getPrice());
		priceLabel.setStyleName("h2");
		
		Button cartButton = new Button();
		cartButton.addClickListener(event -> changeCartButtonStyle(cartButton));
		setCartButtonStyle(cartButton);
		
		VerticalLayout LotDescription = new VerticalLayout(nameLabel, descriptionTextArea, priceLabel, cartButton);
		LotDescription.setSizeFull();
		LotDescription.setComponentAlignment(nameLabel, Alignment.BOTTOM_CENTER);
		LotDescription.setComponentAlignment(descriptionTextArea, Alignment.MIDDLE_CENTER);
		LotDescription.setComponentAlignment(priceLabel, Alignment.MIDDLE_CENTER);
		LotDescription.setComponentAlignment(cartButton, Alignment.MIDDLE_CENTER);
		
		HorizontalLayout root = new HorizontalLayout(photo, LotDescription);
		root.setExpandRatio(photo, (float) 1.2);
		root.setExpandRatio(LotDescription, (float) 0.8);
		root.setComponentAlignment(photo, Alignment.MIDDLE_CENTER);
		root.setSizeFull();
		setContent(root);
		
		this.center();
	}
	
	private void setCartButtonStyle(Button cartButton){
		
		cartButton.setStyleName(ValoTheme.BUTTON_LARGE);
		if(!presenter.isUserBanned()){
			cartButton.setVisible(false);
			return;
		}
		cartButton.setVisible(true);
		if(presenter.isLotInTheCart(lot)){
			cartButton.setCaption("Remove");
			cartButton.setStyleName(ValoTheme.BUTTON_DANGER);
		} else {
			cartButton.setCaption("Add");
			cartButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		}
	}
	
	private void changeCartButtonStyle(Button cartButton){
		if("Add".equals(cartButton.getCaption())){
			cartButton.setCaption("Remove");
			cartButton.setStyleName(ValoTheme.BUTTON_DANGER);
			presenter.addToCart(lot);
		} else {
			cartButton.setCaption("Add");
			cartButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			presenter.removeFromCart(lot);
		}
	}

}
