package org.tuatara.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.tuatara.domain.Lot;
import org.tuatara.mapper.LotMapper;
import org.tuatara.service.MyBatisUtil;

public class LotDAO implements LotMapper{

	@Override
	public List<Lot> getAllLots() {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			List<Lot> lots = lotMapper.getAllLots();
			return lots;
		}
	}

	@Override
	public List<Lot> getLotsByCategory(Integer categoryId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			List<Lot> lots = lotMapper.getLotsByCategory(categoryId);
			return lots;
		}
	}

	@Override
	public List<Lot> getCart(Integer userId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			List<Lot> lots = lotMapper.getCart(userId);
			return lots;
		}
	}
	
	/*
	private void selectLotTrigger(Lot lot){
		lot.setCategory(new CategoryDAO().getCategory(lot));
	}
	*/
	
	@Override
	public void insertLot(Lot lot) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			lotMapper.insertLot(lot);
			sqlSession.commit();
		}
	}

	@Override
	public void insertIntoTheCart(int userId, int lotId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			lotMapper.insertIntoTheCart(userId, lotId);
			sqlSession.commit();
		}
	}
	
	@Override
	public void updateLot(Lot lot) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			lotMapper.updateLot(lot);
			sqlSession.commit();
		}
	}

	@Override
	public void deleteLot(Lot lot) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			lotMapper.deleteLot(lot);
			sqlSession.commit();
		}
	}
	
	@Override
	public void deleteLotFromCart(int userId, int lotId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LotMapper lotMapper = sqlSession.getMapper(LotMapper.class);
			lotMapper.deleteLotFromCart(userId, lotId);
			sqlSession.commit();
		}
	}

}
